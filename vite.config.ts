import { defineConfig } from "vite";
import preact from "@preact/preset-vite";
import envCompatible from "vite-plugin-env-compatible";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [preact(), envCompatible({ prefix: "VITE" })],
    resolve: {
        alias: {
            jquery: "gridstack/dist/jq/jquery.js",
            "jquery-ui": "gridstack/dist/jq/jquery-ui.js",
            "jquery.ui": "gridstack/dist/jq/jquery-ui.js",
            "jquery.ui.touch-punch":
                "gridstack/dist/jq/jquery.ui.touch-punch.js",
        },
    },
});
