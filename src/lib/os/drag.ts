export const makeDraggable = (
    elem: HTMLElement | null,
    handle: HTMLElement | null,
    contentElem: HTMLElement | null
) => {
    let pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;

    if (handle && elem) {
        const dragMouseDown = (e: MouseEvent) => {
            e = e || window.event;

            if (e.target === handle) e.preventDefault();

            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.addEventListener("mouseup", closeDragElement);
            // call a function whenever the cursor moves:
            document.addEventListener("mousemove", elementDrag);
        };

        const elementDrag = (e: MouseEvent) => {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;

            // set the element's new position:
            elem.style.top = elem.offsetTop - pos2 + "px";
            elem.style.left = elem.offsetLeft - pos1 + "px";
        };

        const closeDragElement = () => {
            /* stop moving when mouse button is released:*/
            document.removeEventListener("mouseup", closeDragElement);
            document.removeEventListener("mousemove", elementDrag);
        };

        /* if present, the header is where you move the DIV from:*/
        handle.addEventListener("mousedown", dragMouseDown);
        handle.addEventListener("mouseup", (e: MouseEvent) => {
            const target = e.target as HTMLElement;
            target.style.userSelect = "text";
        });
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        contentElem?.addEventListener("mousedown", (e: MouseEvent) => {
            const target = e.target as HTMLElement;
            if (target !== contentElem) contentElem.style.userSelect = "none";
        });
        contentElem?.addEventListener("mouseup", (e: MouseEvent) => {
            const target = e.target as HTMLElement;
            if (target !== contentElem) contentElem.style.userSelect = "text";
        });
    }
};
