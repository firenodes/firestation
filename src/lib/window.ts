import { VNode } from "preact";
import { Position2D } from "./math";

export interface WindowData {
    name: string;
    type: string;
    pos: Position2D;
    size: Position2D;
    content: VNode | string;
}

export const titleCase = (str: string) =>
    str
        .split(" ")
        .map((w) => w[0].toUpperCase() + w.substr(1).toLowerCase())
        .join(" ");
