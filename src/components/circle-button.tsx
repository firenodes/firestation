import { Flex, FlexProps } from "@chakra-ui/react";
import { RenderableProps } from "preact";
import { JSX } from "preact/compat";
type CircleButtonMode = "red" | "green" | "yellow";

export interface CircleButtonProps {
    colorMode?: CircleButtonMode;
}

export const CircleButton = (
    props: RenderableProps<CircleButtonProps & FlexProps & JSX.HTMLAttributes>
) => {
    return (
        <Flex
            borderRadius="50%"
            w="20px"
            h="20px"
            justifyContent="center"
            alignItems="center"
            bg={
                props.colorMode === "green"
                    ? "#27C93F"
                    : props.colorMode === "red"
                    ? "#FF5F56"
                    : "#FFBD2E"
            }
            {...props}
        ></Flex>
    );
};
