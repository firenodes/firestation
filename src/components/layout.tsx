import { ChakraProvider, ColorModeProvider } from "@chakra-ui/react";
import { Route } from "wouter";
import { useEffect, useState } from "preact/hooks";
import { theme } from "../lib/theme";
import { WindowData } from "../lib/window";
import { WindowManager } from "./window-manager";

export const Layout = () => {
    const [windows, setWindows] = useState<WindowData[]>([
        {
            name: "terminal 1",
            type: "terminal",
            pos: { x: 0, y: 0 },
            size: { x: 300, y: 200 },
            content: "Hello, world!",
        },
        {
            name: "terminal 2",
            type: "terminal",
            pos: { x: 0, y: 0 },
            size: { x: 300, y: 200 },
            content: "This is another terminal window.",
        },
        {
            name: "terminal 3",
            type: "terminal",
            pos: { x: 0, y: 0 },
            size: { x: 300, y: 200 },
            content: "Goodbye, world!",
        },
    ]);

    return (
        <ChakraProvider theme={theme}>
            <ColorModeProvider options={theme.config}>
                <Route path="/">
                    <WindowManager windows={windows} />
                </Route>
            </ColorModeProvider>
        </ChakraProvider>
    );
};
