import { Flex } from "@chakra-ui/react";
import { WindowData } from "../lib/window";
import { WindowBox } from "./window/window";

export const WindowManager = (props: { windows: WindowData[] }) => {
    const drop = (e: DragEvent & { target: HTMLElement }) => {
        e.preventDefault();
        const id = e.dataTransfer?.getData("dragId");
        if (id) {
            const elem = document.getElementById(id);
            if (elem) {
                elem.style.display = "flex";
                e.target.appendChild(elem);
            }
        }
    };

    const dragOver = (e: DragEvent) => e.preventDefault();

    return (
        <Flex
            flexDir="row"
            w="100%"
            h="100%"
            onDrop={drop}
            onDragOver={dragOver}
        >
            {props.windows.map((w) => (
                <WindowBox key={w.name} id={w.name} window={w} />
            ))}
        </Flex>
    );
};
