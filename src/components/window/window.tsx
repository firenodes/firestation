import { Flex, FlexProps, Heading } from "@chakra-ui/react";
import { Component, JSX } from "preact/compat";
import { createRef, RefObject, RenderableProps } from "preact";
import { Position2D } from "../../lib/math";
import { titleCase, WindowData } from "../../lib/window";
import { makeDraggable } from "../../lib/os/drag";
import { WindowControls } from "./controls";

export interface WindowProps {
    window: WindowData;
}

export class WindowBox extends Component<
    RenderableProps<WindowProps & FlexProps & JSX.HTMLAttributes>
> {
    domElement: RefObject<HTMLDivElement>;

    constructor(props: WindowProps) {
        super(props);
        this.domElement = createRef<HTMLDivElement>();
    }

    componentDidMount() {
        if (this.domElement.current)
            makeDraggable(
                this.domElement.current,
                this.domElement.current.querySelector<HTMLElement>(
                    ".window-handle"
                ),
                this.domElement.current.querySelector<HTMLElement>(
                    ".window-content"
                )
            );
    }

    render() {
        return (
            <Flex
                ref={this.domElement}
                position="absolute"
                id={this.props.title}
                justifyContent="flex-start"
                alignItems="center"
                flexDir="column"
                minW="20em"
                minH="20em"
                bg="#212121"
                {...this.props}
            >
                <Flex
                    pl="2em"
                    flexDir="row"
                    w="100%"
                    justifyContent="flex-start"
                    alignItems="center"
                    className="window-handle"
                    bg="#121212"
                >
                    <Heading as="h3" fontSize="xl">
                        {titleCase(this.props.window.name)}
                    </Heading>
                    <WindowControls windowId={this.props.window.name} />
                </Flex>
                <Flex
                    flexDir="column"
                    className="window-content"
                    p="1.5em"
                    justifyContent="flex-start"
                    alignItems="center"
                >
                    {this.props.window.content}
                </Flex>
            </Flex>
        );
    }
}
