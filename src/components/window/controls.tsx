import { CircleButton } from "../circle-button";
import { Flex } from "@chakra-ui/react";

export const WindowControls = (props: { windowId: string }) => (
    <Flex
        p="2em"
        w="100%"
        maxH="1.5em"
        justifyContent="flex-end"
        alignItems="center"
    >
        <CircleButton
            onClick={() => {
                if (document.getElementById(props.windowId))
                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                    document.getElementById(
                        props.windowId
                    )!.hidden = !document.getElementById(props.windowId)
                        ?.hidden;
            }}
            colorMode="yellow"
            ml="0.5em"
        />
        <CircleButton colorMode="green" ml="0.5em" />
        <CircleButton
            onClick={() => document.getElementById(props.windowId)?.remove()}
            colorMode="red"
            ml="0.5em"
        />
    </Flex>
);
